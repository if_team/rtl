# RTL - Show TV

* [Install](#how-to-run)
* [Tests](#tests)
* [Libraries](#libraries)
* [Structure](#structure)

## How to run

Clone the repository
```
$ git clone https://foudouh@bitbucket.org/if_team/rtl.git
```

Install dependencies
```
$ yarn
```

Start development server
```
$ yarn start
```

Production build
```
$ yarn build
```

## Tests
Tests coverage is **20%**, (just to show you some test skills :D) you can find all the tests whithin the `./tests` folder.
I used `Jest` and `Enzymer`.

- To run the tests:
```
$ yarn test
```

## Libraries
- **React** for the UI
- **Redux** for the state managment
- **Lodash** as _Helper_

# Structure
Within each component folder you'll find three fils:

- `Componenet.js` stateless component (functional component)
- `ComponenetContainer.js` Where the managing state/ connection to store.. logic belongs.
- `Componenet.scss` Style
- `index.js` just to keep the path/to/component as small as possible
