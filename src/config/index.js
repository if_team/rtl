export default {
    // This data is normally provided by the backend.
    __API_CONFIG__: {
        BASE_URL: 'http://api.tvmaze.com/',
        SHOWS_ENDPOINT: 'shows/{showId}',
        EPISODES_ENDPOINT: 'shows/{showId}/episodes',
        EPISODE_ENDPOINT: 'shows/{showId}/episodebynumber?season={season}&number={number}',
    },
    __APP_CONFIG__: {
        showId: 6771,
    }
}