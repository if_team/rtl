import compose from 'lodash/fp/compose';
import TVMazeApi from '../TVMazeApi';

export const LOAD_SHOW_START = 'LOAD_SHOW_START';
export const LOAD_SHOW_SUCCESS = 'LOAD_SHOW_SUCCESS';
export const LOAD_SHOW_FAILURE = 'LOAD_SHOW_FAILURE';

export const loadShowStart = () => ({
  type: LOAD_SHOW_START
});

export const loadShowSuccess = show => ({
  type: LOAD_SHOW_SUCCESS,
  data: { show }
});

export const loadShowFailure = error => ({
  type: LOAD_SHOW_FAILURE,
  data: { error }
});

export const loadShowById = (showId) => dispatch => {
    // I want to show some loading indicator (E.g. Spinner) so we need this action to be dispatched!
    dispatch(loadShowStart());

    return TVMazeApi
        .getShowById(showId)
        .then(({ data }) => {
          // Dispatch action to store the fetched show in state.
          compose(dispatch, loadShowSuccess)(data);
        })
        // Error object is so big! and we want to keep our state object as flat as possible and also serializable!
        // Thus let's just add the error message to our state.
        .catch(error => compose(dispatch, loadShowFailure)(error.message));
}