import compose from 'lodash/fp/compose';
import TVMazeApi from '../TVMazeApi';

export const LOAD_EPISODE_START = 'LOAD_EPISODE_START';
export const LOAD_EPISODE_SUCCESS = 'LOAD_EPISODE_SUCCESS';
export const LOAD_EPISODE_FAILURE = 'LOAD_EPISODE_FAILURE';

export const loadEpisodeStart = () => ({
  type: LOAD_EPISODE_START
});

export const loadEpisodeSuccess = episode => ({
  type: LOAD_EPISODE_SUCCESS,
  data: { episode }
});

export const loadEpisodeFailure = error => ({
  type: LOAD_EPISODE_FAILURE,
  data: { error }
});

export const loadEpisodeByNumber = (showId, season, number) => dispatch => {
    // I want to show some loading indicator (E.g. Spinner) so we need this action to be dispatched!
    dispatch(loadEpisodeStart());

    return TVMazeApi
        .getEpisodeByNumber(showId, season, number)
        .then(({ data }) => {
          // Dispatch action to store the fetched show in state.
          compose(dispatch, loadEpisodeSuccess)(data);
        })
        // Error object is so big! and we want to keep our state object as flat as possible and also serializable!
        // Thus let's just add the error message to our state.
        .catch(error => compose(dispatch, loadEpisodeFailure)(error.message));
}