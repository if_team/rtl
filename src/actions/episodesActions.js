import compose from 'lodash/fp/compose';
import TVMazeApi from '../TVMazeApi';

export const LOAD_EPISODES_START = 'LOAD_EPISODES_START';
export const LOAD_EPISODES_SUCCESS = 'LOAD_EPISODES_SUCCESS';
export const LOAD_EPISODES_FAILURE = 'LOAD_EPISODES_FAILURE';

export const loadEpisodesStart = () => ({
  type: LOAD_EPISODES_START
});

export const loadEpisodesSuccess = episodes => ({
  type: LOAD_EPISODES_SUCCESS,
  data: { episodes }
});

export const loadEpisodesFailure = error => ({
  type: LOAD_EPISODES_FAILURE,
  data: { error }
});

export const loadEpisodesByShowId = showId => dispatch => {
    // I want to show some loading indicator (E.g. Spinner) so we need this action to be dispatched!
    dispatch(loadEpisodesStart());

    return TVMazeApi
        .getEpisodesByShowId(showId)
        .then(({ data }) => {
          // Dispatch action to store the fetched show in state.
          compose(dispatch, loadEpisodesSuccess)(data);
        })
        // Error object is so big! and we want to keep our state object as flat as possible and also serializable!
        // Thus let's just add the error message to our state.
        .catch(error => compose(dispatch, loadEpisodesFailure)(error.message));
}
