import PropTypes from 'prop-types';
import Show from './Show';
import Episode from './Episode';

export default Object.assign({}, PropTypes, {
    Show,
    Episode,
});
