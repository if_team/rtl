import PropTypes from 'prop-types';

export default PropTypes.shape({
  id: PropTypes.number,
  name: PropTypes.string,
  summary: PropTypes.string,
  image: PropTypes.shape({
    medium: PropTypes.string,
    original: PropTypes.string
  }),
});
