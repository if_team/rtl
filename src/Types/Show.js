import PropTypes from 'prop-types';

export default PropTypes.shape({
  id: PropTypes.number,
  summary: PropTypes.string,
  image: PropTypes.shape({
    medium: PropTypes.string,
    original: PropTypes.string
  }),
});
