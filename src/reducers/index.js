import { combineReducers } from 'redux';

// --- reducers ---
import show from './show';
import episodes from './episodes';
import episode from './episode';

export default combineReducers({
    show,
    episode,
    episodes,
});
