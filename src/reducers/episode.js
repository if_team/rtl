import {
    LOAD_EPISODE_FAILURE,
    LOAD_EPISODE_START,
    LOAD_EPISODE_SUCCESS
} from '../actions/episodeActions';

const initialState = {
    data: null,
    isEpisodeLoading: false,
    error: null,
};

const episode = (state = initialState, action = {}) => {
  switch (action.type) {
    case LOAD_EPISODE_FAILURE:
        return Object.assign({}, initialState, {
            isEpisodeLoading: false,
            error: action.data.error,
        });

    case LOAD_EPISODE_START:
        return Object.assign({}, initialState, {
            isEpisodeLoading: true,
        });

    case LOAD_EPISODE_SUCCESS:
        return Object.assign({}, initialState, {
            data: action.data.episode,
            isEpisodeLoading: false,
        });

    default:
      return state;
  }
};

export default episode;
