import {
    LOAD_SHOW_FAILURE,
    LOAD_SHOW_START,
    LOAD_SHOW_SUCCESS
} from '../actions/showActions';

const initialState = {
    data: null,
    isShowLoading: false,
    error: null,
};

const show = (state = initialState, action = {}) => {
  switch (action.type) {
    case LOAD_SHOW_FAILURE:
        return Object.assign({}, initialState, {
            isShowLoading: false,
            error: action.data.error,
        });

    case LOAD_SHOW_START:
        return Object.assign({}, initialState, {
            isShowLoading: true,
        });

    case LOAD_SHOW_SUCCESS:
        return Object.assign({}, initialState, {
            data: action.data.show,
            isShowLoading: false,
        });

    default:
      return state;
  }
};

export default show;
