import {
    LOAD_EPISODES_FAILURE,
    LOAD_EPISODES_START,
    LOAD_EPISODES_SUCCESS
} from '../actions/episodesActions';

const initialState = {
    data: null,
    isEpisodesLoading: false,
    error: null,
};

const episodes = (state = initialState, action = {}) => {
  switch (action.type) {
    case LOAD_EPISODES_FAILURE:
        return Object.assign({}, initialState, {
            isEpisodesLoading: false,
            error: action.data.error,
        });

    case LOAD_EPISODES_START:
        return Object.assign({}, initialState, {
            isEpisodesLoading: true,
        });

    case LOAD_EPISODES_SUCCESS:
        return Object.assign({}, initialState, {
            data: action.data.episodes,
            isEpisodesLoading: false,
        });

    default:
      return state;
  }
};

export default episodes;
