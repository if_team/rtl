import axios from 'axios';
import config from '../config';

class TVMazeApi {
    constructor(config) {
        this.config = config;
    }

    /**
     * Get show by ID.
     * @param {String} showId
     * @param {Promise}
     */
    getShowById(showId) {
        const { BASE_URL, SHOWS_ENDPOINT } = this.config;
        return axios.get(BASE_URL + SHOWS_ENDPOINT.replace('{showId}', showId));
    }

    /**
     * Get show by ID.
     * @param {String} showId
     * @param {Promise}
     */
    getEpisodesByShowId(showId) {
        const { BASE_URL, EPISODES_ENDPOINT } = this.config;
        return axios.get(BASE_URL + EPISODES_ENDPOINT.replace('{showId}', showId));
    }

    /**
     * Get Episode by number.
     * @param {String} showId
     * @param {Promise}
     */
    getEpisodeByNumber(showId, season, number) {
        const { BASE_URL, EPISODE_ENDPOINT } = this.config;
        return axios.get(
            BASE_URL + EPISODE_ENDPOINT.replace('{season}', season).replace('{number}', number).replace('{showId}', showId)
        );
    }
    
}

export default new TVMazeApi(config.__API_CONFIG__);