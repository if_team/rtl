import React from 'react';

const handleErrors = Component => {

  class WithErrorsHandled extends React.Component {
    render() {
        if (isShowLoading) {
            return <Spinner />
        } else if (show) {
            return (<Component {...this.props} />);
        } else {
            return <ErrorMessage message={error ? error : 'Something went wrong !!'} />
        }
    }
  }

  return WithErrorsHandled;
};

export default handleErrors;
