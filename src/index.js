import React from "react";
import ReactDOM from "react-dom";
import { BrowserRouter } from 'react-router-dom'

import storeProvider from './hoc/storeProvider';
import configureStore from './store/configureStore';
import config from './config';

// --- Main component ---
import Main from './components/Main';

// --- Common styles ---
import './scss/common.scss';

// --- Configure the store ---
const initialState = {};
const store = configureStore(initialState);
const provideStore = storeProvider(store);
const MainWithStore = provideStore(Main);

const __APP_CONFIG__ = config.__APP_CONFIG__;

ReactDOM.render((
    <BrowserRouter>
        <MainWithStore {...__APP_CONFIG__} />
    </BrowserRouter>
), document.getElementById("root"));
