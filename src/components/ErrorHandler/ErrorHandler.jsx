import React from 'react';
import Types from '../../Types';
import Spinner from '../Spinner';
import ErrorMessage from '../ErrorMessage';

/**
 * @description ErrorHandler component!
 * @param {React.Props<Object>} Props 
 * @returns {React.Component}
 */
const ErrorHandler = ({
    isLoading,
    error,
    children,
}) => {
    if (isLoading) {
        return <Spinner />
    } else if(error) {
        return <ErrorMessage message={error} />
    } else {
        return children;
    }
};

ErrorHandler.propTypes = {
  isLoading: Types.bool,
  error: Types.string,
};

export default ErrorHandler;