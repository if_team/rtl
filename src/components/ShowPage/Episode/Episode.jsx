import React from 'react';
import moment from 'moment';
import { Link } from 'react-router-dom'
import Types from '../../../Types';

import "./Episode.scss";

/**
 * @description Episode component
 * @param {React.Props<Object>} Props 
 * @returns {React.Component}
 */
const Episode = ({
    episode,
}) => (
  <div className="Episode">
    <p className="Episode__title">
        <Link to={`/espisode/${episode.season}/${episode.number}`}>{episode.name}</Link>
    </p>

    <div className="Episode__time">
        {moment(episode.airdate).format('LL')}
    </div>
  </div>
);

Episode.propTypes = {
  Episode: Types.Episode,
};

export default Episode;