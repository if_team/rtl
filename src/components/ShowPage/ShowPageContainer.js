import React from 'react';
import { connect } from 'react-redux';
import compose from 'lodash/fp/compose';

import Types from '../../Types';
import { loadShowById } from '../../actions/showActions';
import { loadEpisodesByShowId } from '../../actions/episodesActions';
import ShowPage from './ShowPage';
import ErrorHandler from '../ErrorHandler';

class ShowPageContainer extends React.Component {
  componentDidMount() {
    this.props.fetchShowById(this.props.showId);
    // We need to fetch the episodes also.
    this.props.fetchEpisodesByShowId(this.props.showId);
  }

  render() {
      const { isShowLoading, errorLoadingShow, show, episodes } = this.props;
      if (!isShowLoading && !errorLoadingShow && !show) {
        return null
      }

      return (
        <ErrorHandler isLoading={isShowLoading} error={errorLoadingShow}>
          <ShowPage show={show} episodes={episodes} />
        </ErrorHandler>
      );
  }
}

ShowPageContainer.propTypes = {
  show: Types.Show,
  showId: Types.number,
  fetchShowById: Types.func,
  fetchEpisodesByShowId: Types.func,
};

const mapStateToProps = state => {
    const show = state.show.data;
    const episodes = state.episodes.data;

    return {
        show,
        episodes,
        isShowLoading: state.show.isShowLoading,
        isEpisodesLoading: state.episodes.isEpisodesLoading,
        errorLoadingShow: state.show.error,
        errorLaodingEpisodes: state.episodes.error,
    };
};

const mapDispatchToProps = dispatch => ({
  fetchShowById: compose(dispatch, loadShowById),
  fetchEpisodesByShowId: compose(dispatch, loadEpisodesByShowId),
});


export default connect(mapStateToProps, mapDispatchToProps)(ShowPageContainer);
