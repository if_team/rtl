import React from 'react';
import Episodes from './Episodes';
import Types from '../../Types';

import "./ShowPage.scss";

/**
 * @description ShowPage component is the root component for the show page!
 * @param {React.Props<Object>} Props 
 * @returns {React.Component}
 */
const ShowPage = ({
    show,
    episodes,
}) => (
  <div className="ShowPage">
    <h1 className="ShowPage__title">{show.name}</h1>

    <section className="Show__header">
      <div className="Show__cover">
        <img src={show.image.medium} alt={show.name} className="Show__cover-image" />
      </div>
      <div className="Show__summary" dangerouslySetInnerHTML={{ __html: show.summary }} />
    </section>

    <section className="Episodes__container">
      { episodes ? <Episodes episodes={episodes} /> : null }
    </section>
  </div>
);

ShowPage.propTypes = {
  show: Types.Show,
  Episodes: Types.arrayOf(Types.Episode),
};

export default ShowPage;