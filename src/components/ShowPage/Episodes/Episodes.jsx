import React from 'react';
import Types from '../../../Types';
import Episode from '../Episode';

import "./Episodes.scss";

/**
 * @description Episode component
 * @param {React.Props<Object>} Props 
 * @returns {React.Component}
 */
const Episodes = ({
    episodes,
}) => (
  <div className="Episodes">
    <h2 className="Episodes__title">Previous Episodes</h2>

    <div className="Episodes__header">
        <p className="Episodes__header-title">Episode Name</p>
        <p className="Episodes__header-time">Airdate</p>
    </div>

    {episodes.map(episode => <Episode key={episode.id} episode={episode} />)}
  </div>
);

Episodes.propTypes = {
  Episodes: Types.arrayOf(Types.Episode),
};

export default Episodes;