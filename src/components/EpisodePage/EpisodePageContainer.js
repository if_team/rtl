import React from 'react';
import { connect } from 'react-redux';
import compose from 'lodash/fp/compose';

import Types from '../../Types';
import { loadEpisodeByNumber } from '../../actions/episodeActions';
import EpisodePage from './EpisodePage';
import ErrorHandler from '../ErrorHandler';

class EpisodePageContainer extends React.Component {
  constructor(props){
    super(props);
    this.goBack = this.goBack.bind(this);
  }

  componentDidMount() {
    // Load episodes of the given show id.
    const { number, season } = this.props.match.params;
    this.props.fetchEpisodeByNumber(this.props.showId, season, number);
  }

  goBack(){
    this.props.history.goBack();
  }

  render() {

    const { isEpisodeLoading, errorLaodingEpisode, episode } = this.props;

    if (!isEpisodeLoading && !errorLaodingEpisode && !episode) {
      return null
    }

    return (
      <ErrorHandler isLoading={isEpisodeLoading} error={errorLaodingEpisode}>
        <EpisodePage episode={episode} goBack={this.goBack} />
      </ErrorHandler>
    );
  }
}

EpisodePageContainer.propTypes = {
  episode: Types.Episode,
  isEpisodeLoading: Types.bool,
  errorLaodingEpisode: Types.string,
  fetchEpisodeByNumber: Types.func,
};

const mapStateToProps = state => {
    const episode = state.episode.data;

    return {
        episode,
        isEpisodeLoading: state.episodes.isEpisodesLoading,
        errorLaodingEpisode: state.episodes.error,
    };
};

const mapDispatchToProps = dispatch => ({
  fetchEpisodeByNumber: compose(dispatch, loadEpisodeByNumber),
});

export default connect(mapStateToProps, mapDispatchToProps)(EpisodePageContainer);
