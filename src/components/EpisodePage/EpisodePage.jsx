import React from 'react';
import Types from '../../Types';

import "./EpisodePage.scss";

/**
 * @description EpisodePage component is the root component for the Episodes page!
 * @param {React.Props<Object>} Props 
 * @returns {React.Component}
 */
const EpisodePage = ({
    episode,
    goBack,
}) => (
  <div className="EpisodePage">
    <h1 className="EpisodePage__title">{episode.name}</h1>

    <section className="Episode__header">
      <div className="Episode__cover">
        <img src={episode.image.medium} alt={episode.name} className="Episode__cover-image" />
      </div>
      <div className="Episode__summary" dangerouslySetInnerHTML={{ __html: episode.summary }} />
    </section>

    <button className="EpisodePage__go-back" onClick={goBack}>Go Back</button>
  </div>
);

EpisodePage.propTypes = {
    episodes: Types.arrayOf(Types.Episode),
};

export default EpisodePage;