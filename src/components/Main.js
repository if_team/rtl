import React from 'react';
import { Route, Switch } from 'react-router-dom';
import ShowPage from './ShowPage';
import EpisodePage from './EpisodePage';

const Main = props => (
  <main>
        <Switch>
            <Route exact path='/' render={routerProps => <ShowPage {...routerProps} {...props} />}/>
            <Route path='/espisode/:season/:number' render={routerProps => <EpisodePage {...routerProps} {...props} />}/>
        </Switch>    
  </main>
)

export default Main;
